﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    [SerializeField] GM gm = null;
    [SerializeField] GameObject dragon = null;
    [SerializeField] GameObject archer = null;
    [SerializeField] GameObject knight = null;
    [SerializeField] GameObject village = null;
    [SerializeField] GameObject tower = null;

    public IEnumerator DoTurn()
    {
        yield return new WaitForSeconds(1f);

        // Use money actions (up to 5 actions, just in case)
        int numberOfActions = 0;
        Action buyAction = selector(checkIfPlayerHasMoreTroops, selector(checkIfCanBuyTroop, buyTroop, doNothing), selector(checkIfCanBuyBuildings, selector(checkIfMoreArchersThanTowers, buyTower, buyVillage), doNothing));
        while (buyAction != doNothing && numberOfActions < 5)
        {
            buyAction();
            numberOfActions++;
            buyAction = selector(checkIfPlayerHasMoreTroops, selector(checkIfCanBuyTroop, buyTroop, doNothing), selector(checkIfCanBuyBuildings, selector(checkIfMoreArchersThanTowers, buyTower, buyVillage), doNothing));
            yield return new WaitForSeconds(1f);
        }

        // Troops actions
        foreach (Unit troop in FindObjectsOfType<Unit>())
        {
            if (troop.playerNumber == 1) continue;
            troop.DoAction();
            yield return new WaitForSeconds(1f);
        }

        gm.EndTurn();
    }

    Action selector(Func<bool> condition, Action ifTrue, Action ifFalse) { return condition() ? ifTrue : ifFalse; }

    bool checkIfPlayerHasMoreTroops()
    {
        int playerTroops = 0;
        int enemyTroops = 0;
        foreach (Unit troop in FindObjectsOfType<Unit>())
        {
            if (troop.playerNumber == 1) playerTroops++;
            else enemyTroops++;
        }

        return playerTroops >= enemyTroops;
    }

    bool checkIfCanBuyTroop() { return GM.player2Gold >= 40; }

    bool checkIfCanBuyBuildings() { return GM.player2Gold >= 100; }

    bool checkIfMoreArchersThanTowers()
    { 
        int archers = 0;
        foreach (Unit troop in FindObjectsOfType<Unit>())
        {
            if (troop.playerNumber == 2 && troop.isArcher) archers++;
        }
        
        int towers = 0;
        foreach (Village building in FindObjectsOfType<Village>())
        {
            if (building.playerNumber == 2 && building.IsTower) towers++;
        }

        return archers > towers; 
    }

    void buyTroop()
    {
        List<Vector3> possiblePositions = InfluenceMapControl.influenceMap.GetPositionsWithAIInfluence();
        if (possiblePositions.Count == 0) return;

        SetCreatableTiles();
        Tile[] tilesArray = FindObjectsOfType<Tile>();
        List<Tile> tiles = new List<Tile>();
        tiles.AddRange(tilesArray);

        Vector3 randomPos = possiblePositions[UnityEngine.Random.Range(0, possiblePositions.Count)];
        Node randomNode = null;
        try { randomNode = Pathfinding.Grid.NodeFromPosition(randomPos); } catch(Exception e) { Debug.Log("BuyTroop: " + e); return; }
        int tries = 0;
        while (tiles.Find(x => x.node == randomNode).isCreatable == false && tries < 5)
        {
            possiblePositions.Remove(randomPos);

            if (possiblePositions.Count == 0) tries = 5;
            else
            {
                randomPos = possiblePositions[UnityEngine.Random.Range(0, possiblePositions.Count)];
                try { randomNode = Pathfinding.Grid.NodeFromPosition(randomPos); } catch(Exception e) { Debug.Log("BuyTroop: " + e); return; }
                tries++;
            }
        }

        if (!tiles.Find(x => x.node == randomNode).isCreatable) return;
        
        GameObject troop = null;
        if (GM.player2Gold >= 80)
        {
            troop = dragon;
            GM.player2Gold -= 80;
        }
        else if (GM.player2Gold >= 70)
        {
            troop = archer;
            GM.player2Gold -= 70;
        }
        else if (GM.player2Gold >= 40)
        {
            troop = knight;
            GM.player2Gold -= 40;
        }
        else return;

        GameObject unit = Instantiate(troop, tiles.Find(x => x.node == randomNode).transform.position, Quaternion.identity);
        randomNode.Walkable = false;
        unit.GetComponent<Unit>().hasMoved = true;
        unit.GetComponent<Unit>().hasAttacked = true;
        gm.ResetTiles();
    }

    void buyVillage()
    {
        List<Vector3> possiblePositions = InfluenceMapControl.influenceMap.GetPositionsWithAIInfluence();
        if (possiblePositions.Count == 0) return;

        SetCreatableTiles();
        Tile[] tilesArray = FindObjectsOfType<Tile>();
        List<Tile> tiles = new List<Tile>();
        tiles.AddRange(tilesArray);

        Vector3 randomPos = possiblePositions[UnityEngine.Random.Range(0, possiblePositions.Count)];
        Node randomNode = null;
        try { randomNode = Pathfinding.Grid.NodeFromPosition(randomPos); } catch(Exception e) { Debug.Log("BuyVillage: " + e); return; }
        int tries = 0;
        while (tiles.Find(x => x.node == randomNode).isCreatable == false && tries < 5)
        {
            possiblePositions.Remove(randomPos);

            if (possiblePositions.Count == 0) tries = 5;
            else
            {
                randomPos = possiblePositions[UnityEngine.Random.Range(0, possiblePositions.Count)];
                try { randomNode = Pathfinding.Grid.NodeFromPosition(randomPos); } catch(Exception e) { Debug.Log("BuyVillage: " + e); return; }
                tries++;
            }
        }

        if (!tiles.Find(x => x.node == randomNode).isCreatable) return;
        
        if (GM.player2Gold >= 100) GM.player2Gold -= 100;
        else return;

        GameObject building = Instantiate(village, tiles.Find(x => x.node == randomNode).transform.position, Quaternion.identity);
        randomNode.Walkable = false;
        gm.ResetTiles();
    }

    void buyTower()
    {
        List<Vector3> possiblePositions = InfluenceMapControl.influenceMap.GetPositionsWithAIInfluence();
        if (possiblePositions.Count == 0) return;

        SetCreatableTiles();
        Tile[] tilesArray = FindObjectsOfType<Tile>();
        List<Tile> tiles = new List<Tile>();
        tiles.AddRange(tilesArray);

        Vector3 randomPos = possiblePositions[UnityEngine.Random.Range(0, possiblePositions.Count)];
        Node randomNode = null;
        try { randomNode = Pathfinding.Grid.NodeFromPosition(randomPos); } catch(Exception e) { Debug.Log("BuyTower: " + e); return; }
        int tries = 0;
        while (tiles.Find(x => x.node == randomNode).isCreatable == false && tries < 5)
        {
            possiblePositions.Remove(randomPos);

            if (possiblePositions.Count == 0) tries = 5;
            else
            {
                randomPos = possiblePositions[UnityEngine.Random.Range(0, possiblePositions.Count)];
                try { randomNode = Pathfinding.Grid.NodeFromPosition(randomPos); } catch(Exception e) { Debug.Log("BuyTower: " + e); return; }
                tries++;
            }
        }

        if (!tiles.Find(x => x.node == randomNode).isCreatable) return;
        
        if (GM.player2Gold >= 100) GM.player2Gold -= 100;
        else return;

        GameObject building = Instantiate(tower, tiles.Find(x => x.node == randomNode).transform.position, Quaternion.identity);
        randomNode.Walkable = false;
        gm.ResetTiles();
    }

    void doNothing() {}

    void SetCreatableTiles()
    {
        gm.ResetTiles();

        Tile[] tiles = FindObjectsOfType<Tile>();
        foreach (Tile tile in tiles)
        {
            if (tile.isClear())
            {
                tile.SetCreatable();
            }
        }
    }
}