﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Unit : MonoBehaviour
{
    public bool isSelected;
    public bool hasMoved;

    public int tileSpeed;
    public float moveSpeed;

    private GM gm;

    public int attackRadius;
    public bool hasAttacked;
    public List<Unit> enemiesInRange = new List<Unit>();

    public int playerNumber;

    public GameObject weaponIcon;

    // Attack Stats
    public int health;
    public int attackDamage;
    public int defenseDamage;
    public int armor;

    public DamageIcon damageIcon;

    public int cost;

	public GameObject deathEffect;

	private Animator camAnim;

    public bool isKing;
    public bool isPlayer;
    public bool isArcher;
    public bool isOnTower;

	private AudioSource source;

    public Text displayedText;

    // Pathfinding
    Node currentNode;
    Queue<Vector3> path;

    private void Start()
    {
		source = GetComponent<AudioSource>();
		camAnim = Camera.main.GetComponent<Animator>();
        gm = FindObjectOfType<GM>();
        UpdateHealthDisplay();
        currentNode = Pathfinding.Grid.NodeFromPosition(transform.position);
    }

    private void UpdateHealthDisplay ()
    {
        if (isKing)
        {
            if (isPlayer) GM.playerLife.text = health.ToString();
            else GM.enemyLife.text = health.ToString();
        }
    }

    private void OnMouseDown()
    {
        if (GM.playerTurn == 1)
        {        
            ResetWeaponIcon();

            if (isSelected)
            {            
                isSelected = false;
                gm.selectedUnit = null;
                gm.ResetTiles();
            }
            else
            {
                if (playerNumber == GM.playerTurn)
                {
                    if (gm.selectedUnit != null) gm.selectedUnit.isSelected = false;

                    gm.ResetTiles();
                    gm.selectedUnit = this;

                    isSelected = true;
                    if(source != null) source.Play(); // Needs fixes
                    
                    GetWalkableTiles();
                    GetEnemies();
                }
                else if (gm.selectedUnit != null && !hasAttacked) gm.selectedUnit.Attack(this);
            }
        }
    }

    private void OnMouseOver()
    {
        if (GM.playerTurn == 2) return;
        if (Input.GetMouseButtonDown(1)) gm.UpdateInfoPanel(this);
    }



    void GetWalkableTiles()
    {
        if (hasMoved) return;

        Tile[] tiles = FindObjectsOfType<Tile>();

        foreach (Tile tile in tiles)
        {
            if (Mathf.Abs(transform.position.x - tile.transform.position.x) + Mathf.Abs(transform.position.y - tile.transform.position.y) <= tileSpeed)
            { 
                if (tile.isClear()) tile.Highlight();
            }
        }
    }

    void GetEnemies()
    {    
        enemiesInRange.Clear();

        Unit[] enemies = FindObjectsOfType<Unit>();
        foreach (Unit enemy in enemies)
        {
            if (Mathf.Abs(transform.position.x - enemy.transform.position.x) + Mathf.Abs(transform.position.y - enemy.transform.position.y) <= attackRadius)
            {
                if (enemy.playerNumber != playerNumber && !hasAttacked)
                {
                    enemiesInRange.Add(enemy);
                    if (playerNumber == 1) enemy.weaponIcon.SetActive(true);
                }
            }
        }
    }

    public void Move(Node n)
    {
        gm.ResetTiles();
        StartCoroutine(StartMovement(n));       
    }

    void Attack(Unit enemy)
    {
        hasAttacked = true;

        int enemyDamege = attackDamage - enemy.armor;
        int unitDamage = enemy.defenseDamage - armor;

        if (enemyDamege >= 1)
        {
            enemy.health -= enemyDamege;
            enemy.UpdateHealthDisplay();
            DamageIcon d = Instantiate(damageIcon, enemy.transform.position, Quaternion.identity);
            d.Setup(enemyDamege);
        }

        if (transform.tag == "Archer" && enemy.tag != "Archer")
        {
            if (Mathf.Abs(transform.position.x - enemy.transform.position.x) + Mathf.Abs(transform.position.y - enemy.transform.position.y) <= 1) // check is the enemy is near enough to attack
            {
                if (unitDamage >= 1)
                {
                    health -= unitDamage;
                    UpdateHealthDisplay();
                    DamageIcon d = Instantiate(damageIcon, transform.position, Quaternion.identity);
                    d.Setup(unitDamage);
                }
            }
        }
        else
        {
            if (unitDamage >= 1)
            {
                health -= unitDamage;
                UpdateHealthDisplay();
                DamageIcon d = Instantiate(damageIcon, transform.position, Quaternion.identity);
                d.Setup(unitDamage);
            }
        }

        if (enemy.health <= 0)
        {         
            if (deathEffect != null)
            {
				Instantiate(deathEffect, enemy.transform.position, Quaternion.identity);
				camAnim.SetTrigger("shake");
			}

            if (enemy.isKing) gm.ShowVictoryPanel(enemy.playerNumber);

            GetWalkableTiles();
            gm.RemoveInfoPanel(enemy);
            Destroy(enemy.gameObject);
        }

        if (health <= 0)
        {
            if (deathEffect != null)
			{
				Instantiate(deathEffect, enemy.transform.position, Quaternion.identity);
				camAnim.SetTrigger("shake");
			}

			if (isKing)  gm.ShowVictoryPanel(playerNumber);

            gm.ResetTiles();
            gm.RemoveInfoPanel(this);
            Destroy(gameObject);
        }

        gm.UpdateInfoStats();
    }

    public void ResetWeaponIcon()
    {
        Unit[] enemies = FindObjectsOfType<Unit>();
        foreach (Unit enemy in enemies)
        {
            enemy.weaponIcon.SetActive(false);
        }
    }

    IEnumerator StartMovement(Node objective)
    {
        path = Pathfinding.FindPath(transform.position, objective.Position);

        Node lastNode = null;
        if (path.Count > 0)
        {
            int steps = 0;
            currentNode.Walkable = true;

            while (path.Count > 0 && steps < tileSpeed)
            {
                transform.position = Vector3.MoveTowards(transform.position, path.Peek(), Time.deltaTime);
                gm.MoveInfoPanel(this);
                
                if (transform.position == path.Peek())
                {                    
                    steps++;
                    lastNode = Pathfinding.Grid.NodeFromPosition(path.Peek());
                    path.Dequeue();
                }

                yield return null;
            }

            lastNode.Walkable = false;
            currentNode = lastNode;
            hasMoved = true;
        }
    }

    public void DoAction()
    {
        if (hasMoved) return;

        if (gameObject.CompareTag("Archer"))
        {
            Action archerAction = selector(checkIfOnTower, selector(checkIfEnemiesInRange, attack, null), selector(checkIfTowersAvailable, moveToTower, selector(checkIfEnemiesInRange, attack, move)));
            archerAction();
        }
        else if (isKing)
        {
            Action kingAction = selector(checkIfEnemiesInRange, attack, flee);
            kingAction();
            return;
        }

        Action troopAction = selector(checkIfEnemiesInRange, attack, move);
        troopAction();        
    }

    Action selector(Func<bool> condition, Action ifTrue, Action ifFalse) { return condition() ? ifTrue : ifFalse; }

    bool checkIfEnemiesInRange()
    {
        GetEnemies();
        if (enemiesInRange.Count == 0) return false;
        return true;
    }

    bool checkIfTowersAvailable()
    {
        Village[] buildings = FindObjectsOfType<Village>();
        foreach (Village building in buildings)
        {
            if (building.IsTower && !building.HasArcher && building.playerNumber == playerNumber) return true;
        }
        return false;
    }

    bool checkIfOnTower()
    {
        return isOnTower;
    }

    void attack()
    {
        Attack(enemiesInRange[UnityEngine.Random.Range(0, enemiesInRange.Count)]);
    }

    void move()
    {
        Vector3 bestPosition = InfluenceMapControl.influenceMap.GetPositionWithMoreInfluence();

        if (bestPosition != new Vector3(-99, -99, -99)) StartCoroutine(StartMovement(Pathfinding.Grid.NodeFromPosition(bestPosition)));
    }

    void flee()
    {
        Vector3 bestPosition = InfluenceMapControl.influenceMap.GetPositionWithLessInfluence();

        if (bestPosition != new Vector3(-99, -99, -99)) StartCoroutine(StartMovement(Pathfinding.Grid.NodeFromPosition(bestPosition)));
    }

    void moveToTower()
    {
        Village tower = null;
        Village[] buildings = FindObjectsOfType<Village>();
        foreach (Village building in buildings)
        {
            if (Mathf.Abs(transform.position.x - building.transform.position.x) + Mathf.Abs(transform.position.y - building.transform.position.y) <= tileSpeed)
            {
                if (building.IsTower && !building.HasArcher && building.playerNumber == playerNumber)
                {
                    tower = building;
                    break;
                }
            }
        }

        StartCoroutine(StartMovement(Pathfinding.Grid.NodeFromPosition(tower.transform.position)));
        tower.HasArcher = true;
        isOnTower = true;
        attackRadius += 3;
    }
}