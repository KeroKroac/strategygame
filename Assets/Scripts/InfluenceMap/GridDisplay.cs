﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface GridData
{
    int Width {get;}
    int Height {get;}
    float GetValue(int x, int y);
}

public class GridDisplay : MonoBehaviour
{
    MeshRenderer meshRenderer;
    MeshFilter meshFilter;
    Mesh mesh;
    GridData data;

    [SerializeField]
    Material material = null;

    [SerializeField]
    Color neutralColor = Color.white;

    [SerializeField]
    Color positiveColor = Color.red;

    [SerializeField]
    Color positiveColor2 = Color.red;

    [SerializeField]
    Color negativeColor = Color.blue;

    [SerializeField]
    Color negativeColor2 = Color.blue;

    Color[] colors;

    public void SetGridData (GridData d)
    {
        data = d;
    }

    public void CreateMesh(Vector3 blPos, float gridSize)
    {
        mesh = new Mesh();
        mesh.name = name;
        meshFilter = gameObject.AddComponent(typeof(MeshFilter)) as MeshFilter;
        meshRenderer = gameObject.AddComponent(typeof(MeshRenderer)) as MeshRenderer;
        meshFilter.mesh = mesh;
        meshRenderer.material = material;

        float objectHeight = transform.position.y;
        float posX = 0;
        float posZ = 0;

        //Create squares starting at bottom left position from the Grid
        List<Vector3> vertexesList = new List<Vector3>();
        for(int y = 0; y < data.Height; ++y)
        {
            for(int x = 0; x < data.Width; ++x)
            {
                Vector3 bl = new Vector3(posX + (x * gridSize),         objectHeight,   posZ + (y * gridSize)); //Bottom left
				Vector3 br = new Vector3(posX + ((x+1) * gridSize),     objectHeight,   posZ + (y * gridSize)); //Bottom right
				Vector3 tl = new Vector3(posX + (x * gridSize),         objectHeight,   posZ + ((y+1) * gridSize)); //Top left
				Vector3 tr = new Vector3(posX + ((x+1) * gridSize),     objectHeight,   posZ + ((y+1) * gridSize)); //Top right

                vertexesList.Add(bl);
                vertexesList.Add(br);
                vertexesList.Add(tl);
                vertexesList.Add(tr);
            }
        }

        //Colors
        List<Color> colorsList = new List<Color>();
        for(int y = 0; y < data.Height; ++y)
        {
            for(int x = 0; x < data.Width; ++x)
            {
                for(int i = 0; i < 4; i++) colorsList.Add(Color.white);
            }
        }
        colors = colorsList.ToArray();

        //Normals
        List<Vector3> normalsList = new List<Vector3>();
        for(int y = 0; y < data.Height; ++y)
        {
            for(int x = 0; x < data.Width; ++x)
            {
                for(int i = 0; i < 4; i++) normalsList.Add(Vector3.up);
            }
        }

        //UVS
        List<Vector2> uvsList = new List<Vector2>();
        for(int y = 0; y < data.Height; ++y)
        {
            for(int x = 0; x < data.Width; ++x)
            {
                uvsList.Add(new Vector2(0, 0));
				uvsList.Add(new Vector2(1, 0));
				uvsList.Add(new Vector2(0, 1));
				uvsList.Add(new Vector2(1, 1));
            }
        }

        //Triangles
        List<int> trianglesList = new List<int>();
        for(int x = 0; x < vertexesList.Count; x += 4)
        {
            int bl = x; int br = x+1; int tl = x+2; int tr = x+3;
            trianglesList.Add(bl); trianglesList.Add(tl); trianglesList.Add(br);
            trianglesList.Add(tl); trianglesList.Add(tr); trianglesList.Add(br);
        }

        mesh.vertices = vertexesList.ToArray();
        mesh.normals = normalsList.ToArray();
        mesh.uv = uvsList.ToArray();
        mesh.colors = colors;
        mesh.triangles = trianglesList.ToArray();
    }

    void setColor(int x, int y, Color c)
    {
        int idx = ((y * data.Width) + x) * 4;
        colors[idx] = c;
        colors[idx + 1] = c;
        colors[idx + 2] = c;
        colors[idx + 3] = c;
    }

    void Update()
    {
        for(int y = 0; y < data.Height; ++y)
        {
            for(int x = 0; x < data.Width; ++x)
            {
                float value = data.GetValue(x, y);
                Color c = neutralColor;

                if(value > 0.5f) c = Color.Lerp(positiveColor, positiveColor2, (value - 0.5f) / 0.5f);
                else if (value > 0) c = Color.Lerp(neutralColor, positiveColor, value / 0.5f);
                else if (value < -0.5f) c = Color.Lerp(negativeColor, negativeColor2, -(value + 0.5f) / 0.5f);
                else c = Color.Lerp(neutralColor, negativeColor, -value / 0.5f);

                setColor(x, y, c);
            }
        }

        mesh.colors = colors;
    }
}
