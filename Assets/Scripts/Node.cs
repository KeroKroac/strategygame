﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node
{
    public bool Walkable;
    public Vector3 Position;
    public Node Parent;
    public int GridX;
    public int GridY;
    public int GCost;
    public int HCost;

    public Node(bool walkable, Vector3 position, int gridX, int gridY)
    {
        Walkable = walkable;
        Position = position;
        GridX = gridX;
        GridY = gridY;
    }

    public int FCost
    {
        get
        {
            return GCost + HCost;
        }
    }
}