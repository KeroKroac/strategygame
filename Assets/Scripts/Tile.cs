﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    public Node node;

    private SpriteRenderer rend;
    public Color highlightedColor;
    public Color creatableColor;

    public LayerMask obstacles;

    private bool isAvailable;
    public bool isCreatable;

    private GM gm;

    public float amount;
    private bool sizeIncrease;

	private AudioSource source;

    private void Start()
    {
		source = GetComponent<AudioSource>();
        gm = FindObjectOfType<GM>();
        rend = GetComponent<SpriteRenderer>();

        isAvailable = false;
        node = Pathfinding.Grid.NodeFromPosition(transform.position);
    }

    public bool isClear()
    {
        return node.Walkable;
    }

    public void Highlight()
    {		
        rend.color = highlightedColor;
        isAvailable = true;
    }

    public void Reset()
    {
        rend.color = Color.white;
        isAvailable = false;
        Debug.Log("Fix");
        isCreatable = false;
    }

    public void SetCreatable()
    {
        if (!node.Walkable) return;
        rend.color = creatableColor;
        isCreatable = true;
    }

    private void OnMouseDown()
    {
        if (GM.playerTurn == 2) return;

        if (isCreatable && gm.createdUnit != null)
        {
            Unit unit = Instantiate(gm.createdUnit, new Vector3(transform.position.x, transform.position.y, 0), Quaternion.identity);
            unit.hasMoved = true;
            unit.hasAttacked = true;
            gm.ResetTiles();
            gm.createdUnit = null;
        } 
        else if (isCreatable && gm.createdVillage != null)
        {
            Instantiate(gm.createdVillage, new Vector3(transform.position.x, transform.position.y, 0) , Quaternion.identity);
            gm.ResetTiles();
            gm.createdVillage = null;
        }
        else if (gm.selectedUnit != null && isAvailable && node.Walkable) gm.selectedUnit.Move(node);
    }

    private void OnMouseEnter()
    {
        if (GM.playerTurn == 2) return;
        if (isClear())
        {
			source.Play();
			sizeIncrease = true;
            transform.localScale += new Vector3(amount, amount, amount);
        }        
    }

    private void OnMouseExit()
    {
        if (GM.playerTurn == 2) return;
        if (isClear())
        {
            sizeIncrease = false;
            transform.localScale -= new Vector3(amount, amount, amount);
        }

        if (!isClear() && sizeIncrease)
        {
            sizeIncrease = false;
            transform.localScale -= new Vector3(amount, amount, amount);
        }
    }
}