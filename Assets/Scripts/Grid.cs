﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour
{
    public LayerMask UnwalkableMask;
    public Vector2 GridWorldSize;
    public float NodeRadius;

    Node[,] grid;
    float nodeDiameter;
    int gridSizeX, gridSizeY;
    public static Vector2 GridSize;
    public Vector3 positions;
    Objectos[,] objetosMapa;

    [SerializeField] GameObject playerKing = null;
    [SerializeField] GameObject enemyKing = null;
    [SerializeField] GameObject playerBase = null;
    [SerializeField] GameObject enemyBase = null;
    [SerializeField] GameObject rock = null;
    [SerializeField] GameObject smallTree = null;
    [SerializeField] GameObject bigTree = null;
    [SerializeField] GameObject enemyArcherTower = null;
    [SerializeField] GameObject playerArcherTower = null;

    void Awake() 
    { 
        nodeDiameter = NodeRadius * 2;
        gridSizeX = Mathf.RoundToInt(GridWorldSize.x / nodeDiameter);
        gridSizeY = Mathf.RoundToInt(GridWorldSize.y / nodeDiameter);

        createMap();
        createGrid();
        GridSize = GridWorldSize;
    }

    enum Objectos
    {
        NONE,
        PLAYERKING,
        ENEMYKING,
        PLAYERBASE,
        ENEMYBASE,
        SMALLTREE,
        BIGTREE,
        ROCK,
        ENEMYARCHERTOWER,
        PLAYERARCHERTOWER
    }


    void createMap()
    {
        objetosMapa = new Objectos[gridSizeX, gridSizeY];

        // Selección aleatoria para poner la base y el rey
        int base_x = Random.Range(1, gridSizeX / 2 - 1);
        int base_y = Random.Range(1, gridSizeY - 1);

        objetosMapa[base_x, base_y] = Objectos.PLAYERBASE;

        bool placed = false;

        while (placed != true)
        {
            int king_x = Random.Range(base_x - 1, base_x + 2);
            int king_y = Random.Range(base_y - 1, base_y + 2);

            if (king_x != base_x && king_y != base_y)
            {
                objetosMapa[king_x, king_y] = Objectos.PLAYERKING;
                placed = true;
            }
        }

        bool towerPlaced = false;

        while (towerPlaced != true)
        {
            int tower_x = Random.Range(base_x - 1, base_x + 2);
            int tower_y = Random.Range(base_y - 1, base_y + 2);

            if (tower_x != base_x && tower_y != base_y && objetosMapa[tower_x, tower_y] == Objectos.NONE )
            {
                objetosMapa[tower_x, tower_y] = Objectos.PLAYERARCHERTOWER;
                towerPlaced = true;
            }
        }

        //Rellenar mapa aleatoriamente
        for (int x = 0; x < objetosMapa.GetLength(0) / 2; x++)
        {
            for (int y = 0; y < objetosMapa.GetLength(1); y++)
            {
                if (objetosMapa[x, y] == Objectos.NONE)
                {
                   int rnd_object = Random.Range(0, 4);

                   if (rnd_object == 0)
                   {
                       objetosMapa[x, y] = Objectos.SMALLTREE;
                   }
                   else if (rnd_object == 1)
                   {
                       objetosMapa[x, y] = Objectos.BIGTREE;
                   }
                   else if (rnd_object == 2)
                   {
                       objetosMapa[x, y] = Objectos.ROCK;
                   }
                }

            }
        }

        mirrorMap(objetosMapa);
    }

    void mirrorMap(Objectos[,] objetosMapa)
    {
        int maxX = objetosMapa.GetLength(0) - 1;
        int maxY = objetosMapa.GetLength(1) - 1;
        for (int x = 0; x < objetosMapa.GetLength(0) / 2; x++)
        {
            for (int y = 0; y < objetosMapa.GetLength(1); y++)
            {     
                if (objetosMapa[x, y] == Objectos.PLAYERKING)
                {
                    objetosMapa[maxX - x, maxY - y] = Objectos.ENEMYKING;
                }
                else if (objetosMapa[x, y] == Objectos.PLAYERBASE)
                {
                    objetosMapa[maxX - x, maxY - y] = Objectos.ENEMYBASE;
                }
                else if (objetosMapa[x, y] == Objectos.PLAYERARCHERTOWER)
                {
                    objetosMapa[maxX - x, maxY - y] = Objectos.ENEMYARCHERTOWER;
                }
                else
                {
                    objetosMapa[maxX - x, maxY - y] = objetosMapa[x, y];
                }

            }
        }

    }


    // Create the grid using the preset values
    void createGrid()
    {
        grid = new Node[gridSizeX, gridSizeY];
        Vector3 bottomLeft = transform.position - Vector3.right * GridWorldSize.x / 2 - Vector3.up * GridWorldSize.y / 2;

        Vector3 nodePosition = Vector3.one;
        bool walkable = false;

        // Initialize every node
        for (int x = 0; x < gridSizeX; x++)
        {
            for (int y = 0; y < gridSizeY; y++)
            {
                nodePosition = bottomLeft + Vector3.right * (x * nodeDiameter + NodeRadius) + Vector3.up * (y * nodeDiameter + NodeRadius);

                // Place map object
                GameObject objectToPlace = null;
                switch (objetosMapa[x, y])
                {
                    case Objectos.PLAYERKING:
                        objectToPlace = playerKing;
                        break;
                    case Objectos.PLAYERBASE:
                        objectToPlace = playerBase;
                        break;
                    case Objectos.ENEMYKING:
                        objectToPlace = enemyKing;
                        break;
                    case Objectos.ENEMYBASE:
                        objectToPlace = enemyBase;
                        break;
                    case Objectos.SMALLTREE:
                        objectToPlace = smallTree;
                        break;
                    case Objectos.BIGTREE:
                        objectToPlace = bigTree;
                        break;
                    case Objectos.ROCK:
                        objectToPlace = rock;
                        break;
                    case Objectos.ENEMYARCHERTOWER:
                        objectToPlace = enemyArcherTower;
                        break;
                    case Objectos.PLAYERARCHERTOWER:
                        objectToPlace = playerArcherTower;
                        break;
                    default:
                        objectToPlace = null;
                        break;
                }

                walkable = true;
                if (objectToPlace != null)
                {
                    Instantiate(objectToPlace, nodePosition, Quaternion.Euler(0, 0, 0));

                    if (objetosMapa[x, y] == Objectos.PLAYERKING || objetosMapa[x, y] == Objectos.ENEMYKING || objetosMapa[x, y] == Objectos.PLAYERBASE || objetosMapa[x, y] == Objectos.ENEMYBASE || objetosMapa[x, y] == Objectos.BIGTREE || objetosMapa[x, y] == Objectos.ENEMYARCHERTOWER || objetosMapa[x, y] == Objectos.PLAYERARCHERTOWER) walkable = false;
                }

                grid[x, y] = new Node(walkable, nodePosition, x, y);
            }
        }
    }

    // Get surrounding nodes from a node
    public List<Node> GetNeighbours(Node node)
    {
        List<Node> neighbours = new List<Node>();

        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                if (x == 0 && y == 0) continue;

                int checkX = node.GridX + x;
                int checkY = node.GridY + y;

                if (checkX >= 0 && checkX < gridSizeX && checkY >= 0 && checkY < gridSizeY) neighbours.Add(grid[checkX, checkY]);
            }
        }

        return neighbours;
    }

    // Get node from world position
    public Node NodeFromPosition(Vector3 pos)
    {
        int x = Mathf.RoundToInt((gridSizeX - 1) * (pos.x + GridWorldSize.x / 2) / GridWorldSize.x);
        int y = Mathf.RoundToInt((gridSizeY - 1) * (pos.y + GridWorldSize.y / 2) / GridWorldSize.y);

        return grid[x, y];
    }

    // For testing purposes (shows a grid in Editor)
    void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position, new Vector3(GridWorldSize.x, 1, GridWorldSize.y));

        if (grid != null)
        {
            foreach (Node n in grid)
            {
                Gizmos.color = n.Walkable ? Color.white : Color.red;
                Gizmos.DrawCube(n.Position, Vector3.one * nodeDiameter * 0.5f);
            }
        }
    }
}