﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Pathfinding : MonoBehaviour
{
    public static Grid Grid;

    void Awake()
    {
        Grid = GetComponent<Grid>();
    }

    // Get world position as a position from a node
    public static Vector3 GetRealPosition(Vector3 pos)
    {
        return (Grid.NodeFromPosition(pos)).Position;
    }

    // Find path between two positions
    public static Queue<Vector3> FindPath(Vector3 startPos, Vector3 targetPos)
    {
        // Get positions as nodes
        Node startNode = Grid.NodeFromPosition(startPos);
        Node targetNode = Grid.NodeFromPosition(targetPos);

        // Initialize the open and closed set
        List<Node> openSet = new List<Node>();
        HashSet<Node> closedSet = new HashSet<Node>();

        // Start the open set with the first node
        openSet.Add(startNode);

        Node currentNode = null;

        // While there are nodes on the open set, keep looking for a route
        while (openSet.Count > 0)
        {
            // Find node with less distance in the open set
            currentNode = openSet[0];
            for (int i = 1; i < openSet.Count; i++)
            {
                if (openSet[i].FCost < currentNode.FCost || openSet[i].FCost == currentNode.FCost && openSet[i].HCost < currentNode.HCost)
                {
                    currentNode = openSet[i];
                }
            }

            openSet.Remove(currentNode);
            closedSet.Add(currentNode);

            // If the current node is the end node, a path has been found (return it)
            if (currentNode == targetNode) return RetracePath(startNode, currentNode);

            // Get neighbours from current node and add them if they're not on the open set or has a better distance value
            foreach (Node neighbour in Grid.GetNeighbours(currentNode))
            {
                if (neighbour != targetNode)
                {
                    if (!neighbour.Walkable || closedSet.Contains(neighbour)) continue;
                }

                int newCostToNeighbour = currentNode.GCost + GetDistance(currentNode, neighbour);
                if (newCostToNeighbour < neighbour.GCost || !openSet.Contains(neighbour))
                {
                    neighbour.GCost = newCostToNeighbour;
                    neighbour.HCost = GetDistance(neighbour, targetNode);
                    neighbour.Parent = currentNode;

                    if (!openSet.Contains(neighbour)) openSet.Add(neighbour);
                }
            }
        }

        // No path found, return empty list
        return new Queue<Vector3>();
    }

    // Get the path as a route from the starting node to the end node
    public static Queue<Vector3> RetracePath(Node startNode, Node endNode)
    {
        Queue<Vector3> path = new Queue<Vector3>();
        Node currentNode = endNode;

        while (currentNode != startNode)
        {
            path.Enqueue(currentNode.Position);
            currentNode = currentNode.Parent;
        }

        // As path is a route starting at the end node, reverse it
        path = new Queue<Vector3>(path.Reverse());
        
        return path;
    }

    // Get distance value between two nodes
    public static int GetDistance(Node nodeA, Node nodeB)
    {
        int distX = Mathf.Abs(nodeA.GridX - nodeB.GridX);
        int distY = Mathf.Abs(nodeA.GridY - nodeB.GridY);
        
        if (distX > distY) return 14 * distY + 10 * (distX - distY);
        return 14 * distX + 10 * (distY - distX);
    }
}
