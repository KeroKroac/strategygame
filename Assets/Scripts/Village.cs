﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Village : MonoBehaviour
{
    public int goldPerTurn;
    public int playerNumber;
    public int cost;   
    public bool IsTower;
    public bool HasArcher;
}